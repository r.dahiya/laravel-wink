# Laravel 5.7 with Wink
The bare shell project having laravel 5.7 setup with Wink using docker and docker-compose. 

## Setup 
* copy .env.example to .env
* run `docker-compose build` to build the docker envioment for first time.
* run `docker-compose up -d` to bring the envionemnt live
* run `dock-compose exec php composer install`

### Setup database
* create database `winkdb` using phpmyadmin (to access phpmyadmin check path http://localhost:8082)
* run command `docker-compose exec php php artisan migrate` to setup users and wink tables
* run command `docker-compose exec php php artisan wink:migrate` will prompt you default username and password.

### Access Wink
* Open link http://localhost:8080/wink to view wink landing page. 
* login with:
 username: admin@mail.com
 password: as shown in terminal in above step