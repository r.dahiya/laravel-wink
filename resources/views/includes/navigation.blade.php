<nav class="container mx-auto p-4 md:pt-8 main-nav max-w-lg">
    <div class="flex items-center justify-between">
        <a href="{{ route('blog.index') }}" class="no-underline font-sans font-bold text-grey-darkest mr-4 md:mr-8">

                <img src="./logo.png" style="height:50px">

        </a>
        {{--<div>--}}
            {{--<a class="no-underline font-sans font-bold text-grey-darkest mr-4 md:mr-8" href="{{ route('blog.index') }}">Blog</a>--}}
        {{--</div>--}}
    </div>
</nav>
